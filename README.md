![](Screenshot.jpg)

# Substrate Screensaver for Windows

* Visual Studio 2019 with C++ should be able to compile.
* Uses the glut lib, which is very old
* Configure using main application run as an exe file
*  or, right click the main application renamed to an scr file

Slightly modified version of the Original Substrate applet, this version adds curving lines and a more broad color palette.