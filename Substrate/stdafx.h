// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once


#define WIN32_LEAN_AND_MEAN                // Exclude rarely-used stuff from Windows headers
#define _WIN32_WINNT 0x0500 // so the code would compile

#include <Windows.h>
#include <ScrnSave.h>

#include "targetver.h"

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <commctrl.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <glut.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>


// TODO: reference additional headers your program requires here
#include "resource.h"

