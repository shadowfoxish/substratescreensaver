
#include "stdafx.h"

#include "Crack.h"
#include "SandPainter.h"
#include "Substrate.h"
#include <algorithm>
#include <cstdint>

Substrate::Substrate() {
	crackCount = 0;
	cfg_MaxCrackCount = 100;
	cracks = NULL;
	//char szBuff[1024];
	//sprintf(szBuff,"+SS: %i\n", this); 
	//OutputDebugString(szBuff);
}

Substrate::~Substrate() {
	delete grid;
	TearDown();
	//char szBuff[1024];
	//sprintf(szBuff,"~SS: %i\n", this); 
	//OutputDebugString(szBuff);
}

void Substrate::SetPreviewMode() {
	cfg_MaxCrackCount = 15;
}

BOOL Substrate::GetCfg_EnableRoundCracks() {
	return cfg_EnableRoundCracks;
}

BOOL Substrate::GetCfg_EnableDarkMode() {
	return cfg_EnableDarkMode;
}

Grid* Substrate::GetCurrentCrackGrid() {
	return grid;
}

float Substrate::getRandomNumber(float a, float b) {
	float random = ((float) rand()) / (float) RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

void Substrate::DrawText(GLint x, GLint y, char* s, GLfloat r, GLfloat g, GLfloat b) {
	int lines;
	char* p;

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0, grid->width, 0, grid->height, -10, 10);
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	glColor3f(1,1,1);
	int boxHeight = 18;
	int boxWidth = 40;
	//Draw a box over the area, so it blanks it out.
	glBegin(GL_TRIANGLES);
	glVertex2f(x-3,y-3);
	glVertex2f(x-3+boxWidth, y-3);
	glVertex2f(x-3,y+boxHeight-3);

	glVertex2f(x-3,y+boxHeight-3);
	glVertex2f(x-3+boxWidth, y-3);
	glVertex2f(x-3+boxWidth, y+boxHeight-3);

	glEnd();

	glColor3f(r,g,b);
	glRasterPos2i(x, y);
	for(p = s, lines = 0; *p; p++) {
		if (*p == '\n') {
			lines++;
			glRasterPos2i(x, y-(lines*18));
		}
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *p);
	}
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void Substrate::TearDown() {
	for(int i = 0; i < crackCount; i++) {
		delete cracks[i];
	}
	delete [] cracks;
	cracks = NULL;
	crackCount = 0;
}

void Substrate::Begin() {
	glPushMatrix();
	//Fade the existing scene
	glClear(GL_DEPTH_BUFFER_BIT);
	if (cfg_EnableDarkMode) {
		glColor4f(0.0, 0.0, 0.0, 0.8);
	}
	else {
		glColor4f(1.0, 1.0, 1.0, 0.8);
	}
	glBegin(GL_TRIANGLES);

	glVertex3f(0, 0, 1);
	glVertex3f(0, grid->height, 1);
	glVertex3f(grid->width, 0, 1);

	glVertex3f(0, grid->height, 1);
	glVertex3f(grid->width, 0, 1);
	glVertex3f(grid->width, grid->height, 1);

	glEnd();

	glPopMatrix();
	glFlush();

	// erase crack grid
	for (int y = 0; y < grid->height; y++) {
		for (int x = 0; x < grid->width; x++) {
			grid->cgrid[y * grid->width + x] = 10001;
		}
	}

	// make random crack seeds
	for (int k = 0; k < 16; k++) {
		int i = Substrate::getRandomNumber(0,(grid->width * grid->height)-1);
		size_t ii = std::max<size_t>(std::min<size_t>(i, (grid->width * grid->height) - 1), 0);
		grid->cgrid[ii] = (int)Substrate::getRandomNumber(0, 360);
	}

	// make just three cracks
	crackCount = 0;
	for (int k = 0; k < 3; k++) {
		MakeCrack();
	}
}

void Substrate::MakeCrack() {
	if (crackCount < cfg_MaxCrackCount) {
		// make a new crack instance
		cracks[crackCount] = new Crack(this);
		crackCount++;
	} else {
		Begin(); //reset
	}
}

void Substrate::DrawFrame(HDC hDC, HGLRC hRC) {
	// crack all cracks
	for (int n = 0; n < crackCount; n++) {
		Crack* c = cracks[n];
		c->move();
	}

	//Write a count of how many active cracks there are at the moment.
	//char buff[50];
	//sprintf(buff, "%i", crackCount);
	//DrawText(30,30,buff,0,0,0);

	glFlush();
}

void Substrate::SetupScreensaverParms(BOOL enableAntiAliasing, BOOL enableRoundCracks, BOOL enableDarkMode, UINT maxCracks) {
	//Options
	cfg_EnableAntiAliasing = enableAntiAliasing;
	cfg_EnableRoundCracks = enableRoundCracks;
	cfg_EnableDarkMode = enableDarkMode;
	cfg_MaxCrackCount = maxCracks < 10000 && maxCracks > 0 ? maxCracks : 0;

	srand(time(NULL));
	//RECT rect;
	//GetClientRect(hWnd, &rect);
	//initGrid(rect.bottom, rect.right);

	if(cfg_EnableAntiAliasing) {
		glEnable(GL_POINT_SMOOTH);
		glShadeModel(GL_SMOOTH);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	glLineWidth(3.0);
	//glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (cfg_EnableDarkMode) {
		glClearColor(0.0, 0.0, 0.0, 1.0);
	}
	else {
		glClearColor(1.0, 1.0, 1.0, 1.0);
	}
}

void Substrate::SetSize(HWND hWnd, HDC hDC, HGLRC hRC) {
	// Get new window size
	RECT rect;
	GetClientRect(hWnd, &rect);

	if (grid != NULL) {
		delete grid;
	}
	grid = new Grid(rect.bottom, rect.right);
	
	TearDown();
	cracks = new Crack*[cfg_MaxCrackCount];
	for (int i = 0; i < cfg_MaxCrackCount; ++i)
		cracks[i] = NULL;
	crackCount = 0;

	GLfloat aspect = (GLfloat) grid->width / grid->height;
	// Adjust graphics to window size
	glViewport(0, 0, grid->width, grid->height);
	glMatrixMode(GL_PROJECTION);
	glPolygonMode(GL_FRONT, GL_FILL);
	glLoadIdentity();
	//gluPerspective(45.0, aspect, 1.0, 100.0);
	//gluOrtho2D(-0.5*width,0.5*width, -0.5*height, 0.5*height);
	//glOrtho(-0.5*width,0.5*width, -0.5*height, 0.5*height,height,-height);
	glOrtho(0, grid->width, 0, grid->height, -10, 10);
	//glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


