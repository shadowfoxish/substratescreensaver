//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Config.rc
//
#define IDI_ICON1                       102
#define ID_CHK_ENABLE_ROUND_CRACKS      1003
#define ID_CHK_ENABLE_ROUND_CRACKS2     1004
#define ID_CHK_ENABLE_ANTIALIAS         1004
#define ID_CHK_ENABLE_DARKMODE          1005
#define IDC_CHECK1                      1006
#define IDC_EDIT1                       1007
#define ID_TXT_MAXCRACKS                1008
#define DLG_SCRNSAVECONFIGURE           2003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
