//#include <Windows.h>
#include "stdafx.h"

#include "Crack.h"
#include "SandPainter.h"
#include "Substrate.h"

#define NUM_COLORS 10

SandPainter::SandPainter()
{
	g = Substrate::getRandomNumber(0.01,0.1);
	red = 0;
	green = 0;
	blue = 0;
	//initialize a random color
	PickNewColor();
	//char szBuff[1024];
	//sprintf(szBuff,"+SP: %i\n", this); 
	//OutputDebugString(szBuff);
}

SandPainter::~SandPainter()
{
	//char szBuff[1024];
	//sprintf(szBuff,"~SP: %i\n", this); 
	//OutputDebugString(szBuff);
}

/// <summary>
/// TODO: Optimizations;
/// It appears this algorithm redraws the entire point cloud scene over and over again, each frame. As the frame gets
/// more cracks in it, the work increases and slows the simulation way down
/// Level 1) Use GL_LINES instead of GL_Points, and figure out how to do some sort of gradient fill
/// Level 2) Continue using GL_Points, but only draw the new stuff since the previous frame.
/// </summary>
/// <param name="x"></param>
/// <param name="y"></param>
/// <param name="ox"></param>
/// <param name="oy"></param>
void SandPainter::render(float x, float y, float ox, float oy) {
	// modulate gain
	g += Substrate::getRandomNumber(-0.050, 0.050);
	float maxg = 1.0;
	if(g < 0) {
		g = 0;
	}
	if(g > maxg) {
		g = maxg;
	}
    
	// calculate grains by distance
	int grains = int(sqrt((ox-x)*(ox-x)+(oy-y)*(oy-y)));
	//int grains = 64;

	//glBegin(GL_POINTS);
	//// lay down grains of sand (transparent pixels)
	float w = g / (grains - 1);
	//for (int i = 0; i < grains; i++) {
	//	float a = 0.1 - ((float)i / ((float)grains*10.0)); //fades as grains gets farther away
	//	glColor4f(red, green, blue, a);
	//	glVertex2f(ox + (x - ox) * sin(sin(i * w)), oy + (y - oy) * sin(sin(i * w)));
	//}

	glBegin(GL_LINES);
	glColor4f(red, green, blue, 0.1); 
	glVertex2f(ox + (x - ox) * sin(sin(0 * w)), oy + (y - oy) * sin(sin(0 * w)));
	glColor4f(red, green, blue, 0);
	glVertex2f(ox + (x - ox) * sin(sin(grains * w)), oy + (y - oy) * sin(sin(grains * w)));

	glEnd();
	glFlush();
}

void SandPainter::PickNewColor() {
	red = Substrate::getRandomNumber(0,1.0);
	green = Substrate::getRandomNumber(0,1.0);
	blue = Substrate::getRandomNumber(0,1.0);
}