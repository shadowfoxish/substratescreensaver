#pragma once

class Substrate;
class SandPainter;

class Crack
{
public:
	Crack(Substrate *source);
	~Crack();

	void findStart();
	void startCrack(int X, int Y, int T);
	void move();
	void regionColor();

private:
	float x;
	float y;
	float t; //direction of travel in degrees
	float angleDelta;
	Substrate *subs;
	SandPainter *sp;
};

