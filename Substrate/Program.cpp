//Substrate OpenGL screensaver for Windows
//Based on the Substrate Gallery of Computation JAVA Code originally written by J. Tarbell
//Created by Shadowfoxish
//11/11/2012

#include "stdafx.h"
#include "Substrate.h"

#define TIMER 1 
#define IDI_ICON1 102

#pragma warning(disable: 4305 4244) /* disable warnings about conversions between float and double */

//Add visual styles
#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

//Do not add comctl32.lib in the additional dependencies property window.
#pragma comment(lib, "ComCtl32.lib")

typedef struct {
	BOOL EnableRoundCracks;
	BOOL EnableAntiAliasing;
	BOOL EnableDarkMode;
	UINT MaxCracks;
} SubstrateConfig;


HWND hWnd;
int mouseExitCount = 0;
int resetMouseExitCount = 0;
Substrate sub;
SubstrateConfig config;

void closeGL(HWND hWnd, HDC hDC, HGLRC hRC) {
	wglMakeCurrent(NULL, NULL);
	wglDeleteContext(hRC);
	ReleaseDC(hWnd, hDC);
}

//Exits the screensaver
void Quit(HWND hWnd, HDC hDC, HGLRC hRC) {
	KillTimer( hWnd, TIMER );
	closeGL(hWnd, hDC, hRC);
	PostQuitMessage(0);
}

void SetupPixelFormat(HWND hWnd, HDC hDC, HGLRC hRC) {
	PIXELFORMATDESCRIPTOR pfd;
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	//pfd.dwFlags =  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW; //Add draw to window for doing windowed-mode
	pfd.dwFlags =  PFD_SUPPORT_OPENGL; //This screensaver does not require doublebuffering
	pfd.dwLayerMask = PFD_MAIN_PLANE;
	pfd.iPixelType = PFD_TYPE_RGBA; 
	//pfd.iPixelType = PFD_TYPE_COLORINDEX;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 24;
	pfd.cAccumBits = 0;
	pfd.cStencilBits = 0;

	int pixelformat = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, pixelformat, &pfd);
}

// Initialize OpenGL graphics
void InitGraphics(HWND hWnd, HDC &hDC, HGLRC &hRC) {
	hDC = GetDC(hWnd);

	SetupPixelFormat(hWnd, hDC, hRC);

	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);
}

/*This block will run the screensaver in windowed mode (suitable for debugging)*/
/* in the project properties, under Linker>System, select 'Not Set' for 'SubSystem' */
/* When releasing the .scr version, this method must be commented out!*/
/* When running with this method (Windowed Mode) Comment out any reference to fChildPreview, as it will cause some goony linker errors */
#ifdef _DEBUG
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	const LPCSTR appname = TEXT("SubstrateWin");
	static HDC hDC;
	static HGLRC hRC;
	WNDCLASS wndclass;
	MSG      msg;

	// Define the window class
	wndclass.style         = 0;
	wndclass.lpfnWndProc   = (WNDPROC)ScreenSaverProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.hInstance     = hInstance;
	wndclass.hIcon         = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wndclass.hCursor       = LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wndclass.lpszMenuName  = appname;
	wndclass.lpszClassName = appname;

	// Register the window class
	if (!RegisterClass(&wndclass)) return FALSE;

	// Create the window
	hWnd = CreateWindow(
		appname,
		appname,
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (!hWnd) return FALSE;

	// Initialize OpenGL
	InitGraphics(hWnd, hDC, hRC);

	// Display the window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);
	
	sub.SetupScreensaverParms(TRUE, TRUE, FALSE, 100);

	// Event loop
	int i = 0;
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == TRUE)
		{
			if (!GetMessage(&msg, NULL, 0, 0)) return TRUE;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		//sub.DrawFrame(hDC, hRC);
		WaitMessage();
	}

	wglDeleteContext(hRC);
	ReleaseDC(hWnd, hDC);
}
#endif

//This method gets the configuration from the registry (or wherever its stored)
SubstrateConfig GetConfig() {
	HKEY hKey;
	DWORD dwDisposition;
	LONG openRes = RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("SOFTWARE\\InfiniteLoop\\Substrate"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);
	
	SubstrateConfig c;
	DWORD size = sizeof(DWORD);
	BOOL aa = 1;
	BOOL rc = 1;
	BOOL dm = 0;
	UINT mc = 100;

	openRes = RegQueryValueEx(hKey, TEXT("EnableAntiAliasing"), 0, NULL, (LPBYTE)&aa, &size);
	if(openRes != ERROR_SUCCESS) {
		printf("Error reading reg key.");
	}
	openRes = RegQueryValueEx(hKey, TEXT("EnableRoundCracks"), 0, NULL, (LPBYTE)&rc, &size);
	openRes = RegQueryValueEx(hKey, TEXT("EnableDarkMode"), 0, NULL, (LPBYTE)&dm, &size);
	openRes = RegQueryValueEx(hKey, TEXT("MaxCracks"), 0, NULL, (LPBYTE)&mc, &size);
	c.EnableAntiAliasing = aa;
	c.EnableRoundCracks = rc;
	c.EnableDarkMode = dm;
	c.MaxCracks = mc;
	return c;
}

//This method saves the configuration to the registry.
void SaveConfig(SubstrateConfig c) {
	HKEY hKey;
	DWORD dwDisposition;
	LONG openRes = RegCreateKeyEx(HKEY_CURRENT_USER, TEXT("SOFTWARE\\InfiniteLoop\\Substrate"), 0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, &dwDisposition);
	
	if(openRes != ERROR_SUCCESS) {
		printf("Error opening reg key");
	}
	BOOL aa = c.EnableAntiAliasing;
	BOOL rc = c.EnableRoundCracks;
	BOOL dm = c.EnableDarkMode;
	UINT mc = c.MaxCracks;

	openRes = RegSetValueEx(hKey, TEXT("EnableAntiAliasing"), 0, REG_DWORD, (LPBYTE)&aa, sizeof(DWORD));
	if(openRes != ERROR_SUCCESS) {
		printf("Error saving reg key.");
	}
	RegSetValueEx(hKey, TEXT("EnableRoundCracks"), 0, REG_DWORD, (LPBYTE)&rc, sizeof(DWORD));
	RegSetValueEx(hKey, TEXT("EnableDarkMode"), 0, REG_DWORD, (LPBYTE)&dm, sizeof(DWORD));
	RegSetValueEx(hKey, TEXT("MaxCracks"), 0, REG_DWORD, (LPBYTE)&mc, sizeof(DWORD));

	RegCloseKey(hKey);
}

//needed for SCRNSAVE.LIB
BOOL WINAPI ScreenSaverConfigureDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
	switch (message) {
		case WM_INITDIALOG:
			//get configuration from the registry
			config = GetConfig();
			CheckDlgButton(hDlg, ID_CHK_ENABLE_ROUND_CRACKS, config.EnableRoundCracks);
			CheckDlgButton(hDlg, ID_CHK_ENABLE_ANTIALIAS, config.EnableAntiAliasing);
			CheckDlgButton(hDlg, ID_CHK_ENABLE_DARKMODE, config.EnableDarkMode);
			SetDlgItemInt(hDlg, ID_TXT_MAXCRACKS, config.MaxCracks, FALSE);
			return TRUE;
		case WM_COMMAND:
			switch(LOWORD(wParam)) { 
				//handle various cases sent when controls
				//are checked, unchecked, etc

				//when OK is pressed, write the configuration
				//to the registry and end the dialog box
				case IDOK:
					//write configuration
					config.EnableRoundCracks = IsDlgButtonChecked(hDlg,ID_CHK_ENABLE_ROUND_CRACKS);
					config.EnableAntiAliasing = IsDlgButtonChecked(hDlg,ID_CHK_ENABLE_ANTIALIAS);
					config.EnableDarkMode = IsDlgButtonChecked(hDlg, ID_CHK_ENABLE_DARKMODE);
					config.MaxCracks = GetDlgItemInt(hDlg, ID_TXT_MAXCRACKS, NULL, FALSE);
					SaveConfig(config);
					EndDialog(hDlg, LOWORD( wParam ) == IDOK); 
					return TRUE; 

				case IDCANCEL: 
					EndDialog(hDlg, LOWORD( wParam ) == IDOK); 
					return TRUE;   

				//case ID_CHK_ENABLE_ROUND_CRACKS:
					
			} //end command switch
	} //end message switch
	return FALSE; 
}

//needed for SCRNSAVE.LIB
BOOL WINAPI RegisterDialogClasses(HANDLE hInst) {
	InitCommonControls();
	return TRUE;
}

void Restart(HWND hWnd, HDC hDC, HGLRC hRC) {
	sub.SetSize(hWnd, hDC, hRC);
	sub.SetupScreensaverParms(config.EnableAntiAliasing, config.EnableRoundCracks, config.EnableDarkMode, config.MaxCracks);
	sub.Begin();
}

//needed for SCRNSAVE.LIB
//Handle window events and messages
/*Uncomment this block to run the screensaver in windowed mode (suitable for debugging)*/
/* in the project properties, under Linker>System, select 'Windows' for 'SubSystem' */
/* Uncomment the lines referring to fChildPreview when using as a screen saver. */
LONG WINAPI ScreenSaverProc (HWND hWnd, UINT uMsg, WPARAM  wParam, LPARAM  lParam) {
	static HDC hDC;
	static HGLRC hRC;
	//static Substrate sub = Substrate(fChildPreview);
	//FILE* pFile =	fopen("c:\\noofdebug.txt","a");
	//char buff[10];
	//sprintf(buff,"%x\n",uMsg);
	//fputs(buff,pFile);
	//fclose(pFile);

	if(fChildPreview) {
		sub.SetPreviewMode();
	}

	switch(uMsg) {
		case WM_CREATE:
			
			config = GetConfig(); //load config settings 
			if (fChildPreview) {
				/*HWND parentWindow = (HWND)_atoi64(__argv[2]);
				RECT rect;
				GetClientRect(parentWindow, &rect);
				
				WNDCLASSEX wc;

				LPCTSTR lpszClassName = TEXT("SubstrateScreenSaver");
				ZeroMemory(&wc, sizeof(wc));
				wc.cbSize = sizeof(wc);
				wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
				wc.lpfnWndProc = (WNDPROC)ScreenSaverProc;
				wc.cbClsExtra = 0;
				wc.cbWndExtra = 0;
				wc.hInstance = (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE);
				wc.hIcon = NULL;
				wc.hCursor = LoadCursor(NULL, IDC_ARROW);
				wc.hbrBackground = NULL;
				wc.lpszMenuName = NULL;
				wc.lpszClassName = lpszClassName;

				RegisterClassEx(&wc);

				HWND newhWnd = CreateWindow(lpszClassName, TEXT(""), WS_CHILD | WS_VISIBLE, 0, 0, rect.right, rect.bottom,
					parentWindow, NULL, (HINSTANCE)GetWindowLong(hWnd, GWL_HINSTANCE), NULL);

				hDC = GetDC(newhWnd);
				hRC = wglCreateContext(hDC);
				*/
				RECT rect;
				GetClientRect(hWnd, &rect);

				hDC = GetDC(hWnd);
				hRC = wglCreateContext(hDC);
				
				config.MaxCracks = 15;
			}

			InitGraphics(hWnd, hDC, hRC);
			Restart(hWnd, hDC, hRC);
			SetTimer(hWnd, TIMER, 1000 / 75, NULL);
			
			return 0;
		case WM_SIZE:
			KillTimer(hWnd, TIMER);
			Restart(hWnd,hDC,hRC);
			SetTimer(hWnd, TIMER, 1000/75, NULL); 
			//sub.DrawFrame(hDC, hRC);
			return 0;
		case WM_CLOSE: 
			wglMakeCurrent(hDC, NULL);
			wglDeleteContext(hRC);
			DestroyWindow(hWnd);
			return 0;
		case WM_DESTROY:
			Quit(hWnd, hDC, hRC);
			return 0;
		case WM_TIMER:
			sub.DrawFrame(hDC, hRC);
			resetMouseExitCount++;
			if(resetMouseExitCount > 10) {
				resetMouseExitCount = 0;
				mouseExitCount = 0;
			}
			return 0;
		case WM_MOUSEMOVE:
			if(!fChildPreview) {
				if(mouseExitCount < 10) {
					mouseExitCount++;
				} else {
					Quit(hWnd, hDC, hRC);
				}
			}
			return 0;
		case WM_KEYDOWN:
			if(!fChildPreview) {
				Quit(hWnd, hDC, hRC);
			}
			return 0;
		default: // Default event handler
			return DefWindowProc (hWnd, uMsg, wParam, lParam); 
	} 
}
