#include <Windows.h>
#include <ScrnSave.h>
#include <commctrl.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

HWND hWnd;

void InitGraphics(HWND hWnd, HDC &hDC, HGLRC &hRC);
void SetupScreensaverParms(HWND hWnd);
void SetupPixelFormat(HWND hWnd, HDC hDC, HGLRC hRC);
void DrawFrame(HDC hDC, HGLRC hRC);

GLfloat angleX;
GLfloat angleY;


#define PI 3.1415926535898

//Substrate OpenGL screensaver for Windows
//Ported/modified to C++ from the Substrate Gallery of Computation JAVA Code originally written by J. Tarbell
//This version created by Shadowfoxish

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE pPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	const LPCSTR appname = TEXT("SubstrateWin");
	static HDC hDC;
    static HGLRC hRC;
	WNDCLASS wndclass;
	MSG      msg;

	// Define the window class
	wndclass.style         = 0;
	wndclass.lpfnWndProc   = (WNDPROC)ScreenSaverProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.hInstance     = hInstance;
	wndclass.hIcon         = LoadIcon(hInstance, appname);
	wndclass.hCursor       = LoadCursor(NULL,IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wndclass.lpszMenuName  = appname;
	wndclass.lpszClassName = appname;

	// Register the window class
	if (!RegisterClass(&wndclass)) return FALSE;

	// Create the window
	hWnd = CreateWindow(
		appname,
		appname,
		WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	if (!hWnd) return FALSE;

	// Initialize OpenGL
	InitGraphics(hWnd, hDC, hRC);

	// Display the window
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	SetupScreensaverParms(hWnd);

	// Event loop
	int i = 0;
	while (1)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_NOREMOVE) == TRUE)
		{
			if (!GetMessage(&msg, NULL, 0, 0)) return TRUE;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		DrawFrame(hDC, hRC);
		Sleep(60);
		
	}

	wglDeleteContext(hRC);
	ReleaseDC(hWnd, hDC);
}

// Initialize OpenGL graphics
void InitGraphics(HWND hWnd, HDC &hDC, HGLRC &hRC) {
	hDC = GetDC(hWnd);

	SetupPixelFormat(hWnd, hDC, hRC);

	hRC = wglCreateContext(hDC);
	wglMakeCurrent(hDC, hRC);

	//glClearColor(0, 0, 0, 0.5);
	//glClearDepth(1.0);
	//glEnable(GL_DEPTH_TEST);
}

void DrawFrame(HDC hDC, HGLRC hRC) {
	//Setup the initial transform as driven by the keyboard
	glPushMatrix();
	glRotatef(angleX, 1.0,0.0,0.0);
	glRotatef(angleY, 0.0,1.0,0.0);
	glColor4f(1.0,1.0,1.0,1.0);
	GLint circle_points =20;
	glClear(GL_COLOR_BUFFER_BIT);
	double angle = (2 * PI) / circle_points;
	
	//glVertex2f( cos(0.0) , sin(0.0));
	for(int h = 0; h < circle_points; h++) {
		glRotatef(360 / circle_points,0.0,1.0,0.0);
		int i;
		double angle1=0.0;
		glBegin(GL_LINE_LOOP);
		for ( i=0 ; i < circle_points ;i++)
		{
			//printf( "angle = %f \n" , angle1);
			glVertex2f(cos(angle1)*200,sin(angle1)*200);
			angle1 += angle;
		}
		//close the circle
		glVertex2f(cos(2.0*PI)*200,sin(2.0*PI)*200);
		glEnd();
	}

	for(int h = 0; h < circle_points; h++) {
		glRotatef(360 / circle_points,1.0,0.0,0.0);
		int i;
		double angle1=0.0;
		glBegin(GL_LINE_LOOP);
		for ( i=0 ; i < circle_points ;i++)
		{
			//printf( "angle = %f \n" , angle1);
			glVertex2f(cos(angle1)*200,sin(angle1)*200);
			angle1 += angle;
		}
		//close the circle
		glVertex2f(cos(2.0*PI)*200,sin(2.0*PI)*200);
		glEnd();
	}
	glColor4f(0.2,0.3,1.0,1.0);
	glBegin(GL_POINTS);
	for(int i = 0; i < 60; i++) {
		glVertex2f(10*i,10*i);
	}
	glEnd();
	glPopMatrix();
	glFlush();
	
}

void closeGL(HWND hWnd, HDC hDC, HGLRC hRC) {
	wglMakeCurrent( NULL, NULL );
	wglDeleteContext( hRC );
	ReleaseDC( hWnd, hDC );
}


void SetupScreensaverParms(HWND hWnd) {
	RECT rect;
	GetClientRect(hWnd, &rect);
	glClearColor(0.0,0.0,0.0,1.0);
	glEnable(GL_LINE_SMOOTH);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glLineWidth(1.0);
	glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	srand(time(NULL));
}

// Draw frame
// Resize graphics to fit window
// Set up pixel format for graphics initialization
void SetupPixelFormat(HWND hWnd, HDC hDC, HGLRC hRC) {
	PIXELFORMATDESCRIPTOR pfd;
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;
	//pfd.dwFlags =  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; // | PDF_DRAW_TO_WINDOW //Add draw to window for doing windowed-mode
	pfd.dwFlags =  PFD_SUPPORT_OPENGL; //This screensaver does not require doublebuffering
	pfd.dwLayerMask = PFD_MAIN_PLANE;
	pfd.iPixelType = PFD_TYPE_RGBA; 
	//pfd.iPixelType = PFD_TYPE_COLORINDEX;
	pfd.cColorBits = 24;
	pfd.cDepthBits = 24;
	pfd.cAccumBits = 0;
	pfd.cStencilBits = 0;

	int pixelformat = ChoosePixelFormat(hDC, &pfd);
	SetPixelFormat(hDC, pixelformat, &pfd);
}

//Exits the screensaver
void Quit(HWND hWnd, HDC hDC, HGLRC hRC) {
	//KillTimer( hWnd, TIMER );
	closeGL( hWnd, hDC, hRC );
	PostQuitMessage(0);
}

void ResizeGraphics(HWND hWnd, HDC hDC, HGLRC hRC) {
	// Get new window size
	RECT rect;
	GetClientRect(hWnd, &rect);
	int width = rect.right;
	int height = rect.bottom;
	//reshape_noof(width,height);

	GLfloat aspect = (GLfloat) width / height;
	// Adjust graphics to window size
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//gluPerspective(45.0, aspect, 1.0, 100.0);
	//gluOrtho2D(-0.5*width,0.5*width, -0.5*height, 0.5*height);
	glOrtho(-0.5*width,0.5*width, -0.5*height, 0.5*height,height,-height);
	//glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


//needed for SCRNSAVE.LIB
BOOL WINAPI ScreenSaverConfigureDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {
    return FALSE;
}

//needed for SCRNSAVE.LIB
BOOL WINAPI RegisterDialogClasses(HANDLE hInst) {
  return TRUE;
}

void HandleKeyboard(WPARAM wParam) {
	switch(wParam) {
		case VK_UP:
			angleX += 3.6;
			break;
		case VK_DOWN:
			angleX -= 3.6;
			break;
		case VK_LEFT:
			angleY += 3.6;
			break;
		case VK_RIGHT:
			angleY -= 3.6;
			break;
	}
}


//needed for SCRNSAVE.LIB
//Handle window events and messages
LRESULT WINAPI ScreenSaverProc (HWND hWnd, UINT uMsg, WPARAM  wParam, LPARAM  lParam) {
	static HDC hDC;
    static HGLRC hRC;
	//FILE* pFile =	fopen("c:\\noofdebug.txt","a");
	//char buff[10];
	//sprintf(buff,"%x\n",uMsg);
	//fputs(buff,pFile);
	//fclose(pFile);

	switch(uMsg) {
		case WM_CREATE:
			InitGraphics(hWnd,hDC,hRC);
			SetupScreensaverParms(hWnd);
			//SetTimer( hWnd, TIMER, 1000/75, NULL ); 
			return 0;
		case WM_SIZE:
			//KillTimer( hWnd, TIMER );
			ResizeGraphics(hWnd,hDC,hRC);
			SetupScreensaverParms(hWnd);
			//SetTimer( hWnd, TIMER, 1000/75, NULL ); 
			DrawFrame(hDC, hRC);
			return 0;
		case WM_CLOSE: 
			wglMakeCurrent(hDC, NULL);
            wglDeleteContext(hRC);
			DestroyWindow(hWnd);
			return 0;
		case WM_DESTROY:
			Quit( hWnd, hDC, hRC );
			return 0;
		case WM_TIMER:
			DrawFrame(hDC, hRC);
			/*resetMouseExitCount++;
			if(resetMouseExitCount > 10) {
				resetMouseExitCount=0;
				mouseExitCount=0;
			}*/
			return 0;
		case WM_MOUSEMOVE:
			/*if(mouseExitCount < 10) {
				mouseExitCount++;
				return 0;
			} */
		case WM_KEYDOWN:
			HandleKeyboard(wParam);
			//Quit( hWnd, hDC, hRC );
			return 0;
		default: // Default event handler
			return DefWindowProc (hWnd, uMsg, wParam, lParam); 
	} 
}