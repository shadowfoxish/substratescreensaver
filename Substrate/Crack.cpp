#include "stdafx.h"

#include "Substrate.h"
#include "SandPainter.h"

#include "Crack.h"

#define PI 3.14159265359

BOOL isDarkMode;
	
Crack::Crack(Substrate *source)
{
	sp = new SandPainter();
	subs = source;
	isDarkMode = source->GetCfg_EnableDarkMode();

	//randomly decide if we're going to have an angleDelta or not
	int doDelta = Substrate::getRandomNumber(1,10);
	if(source->GetCfg_EnableRoundCracks() == TRUE && doDelta <= 5) {
		angleDelta = Substrate::getRandomNumber(-0.05, 0.05);
	} else {
		angleDelta = 0;
	}

	findStart();
	//char szBuff[1024];
	//sprintf(szBuff,"+CK: %i\n", this); 
	//OutputDebugString(szBuff);
}

Crack::~Crack(void)
{
	delete sp;
	//char szBuff[1024];
	//sprintf(szBuff,"~CK: %i\n", this); 
	//OutputDebugString(szBuff);
}


void Crack::findStart(void) {
	sp->PickNewColor();
	// pick random point
	int px = 0;
	int py = 0;
    
	// shift until crack is found
	bool found = false;
	int timeout = 0;
	Grid* grid = subs->GetCurrentCrackGrid();
	int dimx = grid->width;
	int dimy = grid->height;

	while ((!found) || (timeout++ > 1000)) {

		px = rand() % dimx;
		py = rand() % dimy;

		if (grid->cgrid[py * dimx + px] < 10000) {
			found = true;
		}
	}
    
	if (found) {
		// start crack
		int a = grid->cgrid[py * dimx + px];
		
		if ((rand() % 100) < 50) {
			a -= 90 + Substrate::getRandomNumber(-2.0, 2.1);
		} else {
			a += 90 + Substrate::getRandomNumber(-2.0, 2.1);
		}
		startCrack(px,py,a);
	} else {
		//println("timeout: "+timeout);
	}
}

void Crack::startCrack(int X, int Y, int T) {
	x = X;
    y = Y;
    t = T; //%360;
    x += 0.61 * cos(t * PI / 180);
    y += 0.61 * sin(t * PI / 180);  
}

void Crack::move() {
	// continue cracking
	//Adjust t to get curves.
	t += angleDelta;
	x += 0.42 * cos(t * PI / 180);
	y += 0.42 * sin(t * PI / 180); 
    
	// bound check
	float z = 0.33;
	//float z = 0;
	int cx = int(x + Substrate::getRandomNumber(-z, z));  // add fuzz
	int cy = int(y + Substrate::getRandomNumber(-z, z));
    
	// draw sand painter
	regionColor();
   
	// draw crack
	if (isDarkMode) {
		glColor3f(0.5, 0.5, 0.5);
	}
	else {
		glColor3f(0.0, 0.0, 0.0);
	}
	glBegin(GL_POINTS);
	glVertex2f(x + Substrate::getRandomNumber(-z,z), y + Substrate::getRandomNumber(-z,z));
    glEnd();
	
	Grid* grid = subs->GetCurrentCrackGrid();
	int dimx = grid->width;
	int dimy = grid->height;

	if ((cx >= 0) && (cx < dimx) && (cy >= 0) && (cy < dimy)) {
		// safe to check
		if ((grid->cgrid[cy * dimx + cx] > 10000) || (abs((int)(grid->cgrid[cy * dimx + cx] - t)) < 5)) {
			// continue cracking
			grid->cgrid[cy * dimx + cx] = (int)t;
		} else if (abs((int)(grid->cgrid[cy * dimx + cx] - t)) > 2) {
			// crack encountered (not self), stop cracking
			findStart();
			//subs->makeCrack();
		}
	} else {
		// out of bounds, stop cracking
		findStart();
		subs->MakeCrack();
	}
}

void Crack::regionColor(void) {
	// start checking one step away
	float rx = x;
	float ry = y;
	bool openspace = true;
	Grid* grid = subs->GetCurrentCrackGrid();
	int dimx = grid->width;
	int dimy = grid->height;
	// find extents of open space
	while (openspace) {
		// move perpendicular to crack
		rx += 0.81 * sin(t * PI / 180);
		ry -= 0.81 * cos(t * PI / 180);
		int cx = int(rx);
		int cy = int(ry);
		if ((cx >= 0) && (cx < dimx) && (cy >= 0) && (cy < dimy)) {
			// safe to check
			if (grid->cgrid[cy * dimx + cx] > 10000) {
				// space is open
			} else {
				openspace = false;
			}
		} else {
			openspace = false;
		}
	}

	//char szBuff[1024];
	//sprintf(szBuff,"SP: %i, Crack: %i\n", sp, this );
	//
	//OutputDebugString(szBuff);

	//draw sand painter
	sp->render(rx, ry, x, y);
}