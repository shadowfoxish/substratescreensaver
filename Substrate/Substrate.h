#pragma once

struct Color {
	float red;
	float green;
	float blue;
};

struct Grid {
	Grid(int h, int w) : width(w), height(h) {
		cgrid = new int[h * w];
	}
	~Grid() {
		delete[] cgrid;
		cgrid = NULL;
	}
	int* cgrid;
	int height;
	int width;
};

class Crack;

class Substrate
{
public:
	Substrate();
	~Substrate();
	BOOL GetCfg_EnableRoundCracks();
	BOOL GetCfg_EnableDarkMode();
	Grid* GetCurrentCrackGrid();
	
	static float getRandomNumber(float low, float high);
	void DrawText(GLint x, GLint y, char* s, GLfloat r, GLfloat g, GLfloat b);
	void TearDown();
	void Begin();
	void MakeCrack();
	void DrawFrame(HDC hDC, HGLRC hRC);
	void SetupScreensaverParms(BOOL enableAntiAliasing, BOOL enableRoundCracks, BOOL enableDarkMode, UINT maxCracks);
	void SetSize(HWND hWnd, HDC hDC, HGLRC hRC);
	void SetPreviewMode();
private:
	//Common objects
	HWND hWnd;
	UINT crackCount;
	Grid* grid;

	Crack** cracks;
	BOOL cfg_EnableAntiAliasing;
	BOOL cfg_EnableRoundCracks;
	BOOL cfg_EnableDarkMode;
	UINT cfg_MaxCrackCount;
};