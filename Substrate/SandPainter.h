#pragma once

class SandPainter
{
public:
	SandPainter(void);
	~SandPainter(void);
	void render(float x, float y, float ox, float oy);
	void PickNewColor();
private:
	float g;
	float red; /*if these are made global, the sandpainters all share the same color*/
	float green;
	float blue;
};

